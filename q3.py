POSITIVE_DOCS_FILE_PATH = "pos_Bk"
NEGATIVE_DOCS_FILE_PATH = "neg_Bk"
DATA_PATH = "./data/q3"
import nltk
from os import walk
from os.path import join
from json import load
from sklearn.model_selection import train_test_split
from sentiment_preprocessor import SentimentPreprocessor
from feature_adder import FeatureAdder

def generate_dataset():

    X = []
    Y = []


    for root, _, filenames in walk(join(DATA_PATH,POSITIVE_DOCS_FILE_PATH)):
        for file in filenames:
            X.append(open(join(root, file),'rb').read().decode('ISO-8859-1'))
            Y.append("positive")

    for root, _, filenames in walk(join(DATA_PATH,NEGATIVE_DOCS_FILE_PATH)):
        for file in filenames:
            X.append(open(join(root, file),'rb').read().decode('ISO-8859-1'))
            Y.append("negative")

    return X,Y

X, Y = generate_dataset()

def generate_sklearn_vectorizer(config):
    from sklearn.feature_extraction.text import CountVectorizer

    has_unigram = False
    has_bigram = False
    has_binary = True

    for task in config:
        if list(task.keys())[0] == 'unigram':
            has_unigram = True
        if list(task.keys())[0] == 'bigram':
            has_bigram = True
        if list(task.values())[0] == 2:
            has_binary = False

    if has_bigram and has_unigram and has_binary:
        vectorizer = CountVectorizer(ngram_range=(1,2), binary=True)
    elif has_unigram and has_binary:
        vectorizer = CountVectorizer(binary=True)
    elif has_bigram and has_binary:
        vectorizer = CountVectorizer(ngram_range=(2,2), binary=True)

    elif has_bigram and has_unigram:
        vectorizer = CountVectorizer(ngram_range=(1,2))
    elif has_unigram:
        vectorizer = CountVectorizer()
    elif has_bigram:
        vectorizer = CountVectorizer(ngram_range=(2,2))
    else:
        raise ValueError("wtf")

    return vectorizer

def calculate_prediction_accuracy_metrics(X, Y, config):

    from sklearn.pipeline import Pipeline
    from sklearn.linear_model import LogisticRegression
    from sklearn.naive_bayes import BernoulliNB
    from sklearn.metrics import precision_recall_fscore_support,accuracy_score
    from nltk import NaiveBayesClassifier

    preprocessor = SentimentPreprocessor(config)
    preprocessed_x = preprocessor.transform(X)

    # Classification using nltk
    feature_adder = FeatureAdder(config)
    converted_featuresets = feature_adder.transform(preprocessed_x, Y)
    train_set, test_set = train_test_split(converted_featuresets, test_size=.20, random_state=1337)
    classifier = NaiveBayesClassifier.train(train_set)

    # Classification using sklearn
    x_train, x_test, y_train, y_test = train_test_split(preprocessed_x,Y,test_size=.20,random_state=1337)
    vecto = generate_sklearn_vectorizer(config)
    classif  = LogisticRegression()
    pipeline = Pipeline([('vecto',vecto),('classif',classif)])
    pipeline.fit(x_train,y_train)
    p, r, f, s = precision_recall_fscore_support(y_test, pipeline.predict(x_test))
    a = accuracy_score(y_test, pipeline.predict(x_test))

    return a, p, r

for current_path,current_dir,filenames in walk(join(DATA_PATH,'experiments')):
    print("experiment;accuracy;precision;recall")
    for filename in sorted(filenames):
            config_file = load(open(join(current_path,filename)))
            try:
                a, p, r = calculate_prediction_accuracy_metrics(X, Y,config_file)
                print("{};{};{};{}".format(filename[:-4],a,p[1],r[1]))
            except Exception as e:
                print(e)

# TEST DE DEBUGGAGE DE SKLEARN VS NLTK...

######################
# nltk vs sklearn
######################
#
# a = [x[0].keys() for x in train_set]
# nltk_all_features = set()
# for features in a:
#      for feature in features:
#          nltk_all_features.add(feature)
# nltk_all_stripped_features = [x[10:-1] for x in nltk_all_features]
# sklearn_all_stripped_feature = [x for x in vecto.vocabulary_]
#
# print("Nltk has {} features, sklearn has {} features".format(len(nltk_all_stripped_features),len(sklearn_all_stripped_feature)))
#
# with open("sklearn_missing_features.txt",'w') as output_file:
#     for nltk_feature in nltk_all_stripped_features:
#         if nltk_feature not in sklearn_all_stripped_feature:
#             output_file.write("{}\n".format(nltk_feature))
#
#
# with open("nltk_missing_features.txt",'w') as output_file:
#     for sklearn_feature in sklearn_all_stripped_feature:
#         if sklearn_feature not in nltk_all_stripped_features:
#             output_file.write("{}\n".format(sklearn_feature))


##########################
# using nltk classifier with sklean tokenizer
#########################33

#
# featuresets = []
# transformed_x = vecto.transform(x_train)
#
#
# for document in range(transformed_x.shape[0]):
#     featuresets.append(([x for x in transformed_x[document].indices],y_train[document]))
#
# transformed_x = vecto.transform(x_test)
# for document in range(transformed_x.shape[0]):
#     featuresets.append(([x for x in transformed_x[document].indices],y_test[document]))
#
# converted_featuresets = list(zip([by_pass_feature(x[0]) for x in featuresets],[x[1] for x in featuresets]))

# def by_pass_feature(tupled_sklearn_feature):
#     the_features = {}
#     for word in tupled_sklearn_feature:
#         the_features[word] = the_features.get(word,True)
#
#     return the_features
