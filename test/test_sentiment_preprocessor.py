from unittest import TestCase
from json import load
from os.path import join

from sentiment_preprocessor import SentimentPreprocessor

TEST_DATA_FOLDER = "./test/data/preprocessor"

STEMMING_TEST_FILE = 'stem_test_data.json'
TRANSFORMED_STEMMING_TEST_FILE = 'stem_transformed_test_data.json'

STOPWORD_TEST_FILE = 'stopword_test_data.json'
TRANSFORMED_STOPWORD_TEST_FILE = 'stopword_transformed_test_data.json'

MINMAX_TEST_FILE = 'minmax_test_data.json'
TRANSFORMED_MINMAX_MIN_TEST_FILE = 'minmax_transformed_min_test_data.json'
TRANSFORMED_MINMAX_MAX_TEST_FILE = 'minmax_transformed_max_test_data.json'

POS_TEST_FILE = 'pos_test_data.json'
TRANSFORMED_POS_TEST_FILE = 'pos_transformed_test_data.json'


FITTING_TEST_FILE = 'fitting_test_data.json'


class TestSentimentPreprocessor(TestCase):


    def test_when_config_stemming_then_stem_docs(self):
        config_file = [ {'stemming':True} ]
        processor = SentimentPreprocessor(config_file)
        test_docs = load(open(join(TEST_DATA_FOLDER, STEMMING_TEST_FILE)))

        actual_transformation = processor.transform(test_docs)

        expected_transformation = load(open(join(TEST_DATA_FOLDER, TRANSFORMED_STEMMING_TEST_FILE)))
        self.assertEqual(expected_transformation[0],actual_transformation[0])

    def test_when_config_stopword_then_remove_stopword(self):
        config_file = [{'stopword': True}]
        processor = SentimentPreprocessor(config_file)
        test_docs = load(open(join(TEST_DATA_FOLDER, STOPWORD_TEST_FILE)))

        actual_transformation = processor.transform(test_docs)

        expected_transformation = load(open(join(TEST_DATA_FOLDER, TRANSFORMED_STOPWORD_TEST_FILE)))
        self.assertEqual(expected_transformation[0], actual_transformation[0])

    def test_given_fitted_processor_when_config_min_nb_then_remove_words_with_less_than_min(self):
        config_file = [{'min_df': 5}]
        processor = SentimentPreprocessor(config_file)
        fitting_docs = load(open(join(TEST_DATA_FOLDER, FITTING_TEST_FILE)))
        processor.fit(fitting_docs)

        test_docs = load(open(join(TEST_DATA_FOLDER, MINMAX_TEST_FILE)))

        actual_transformation = processor.transform(test_docs)

        expected_transformation = load(open(join(TEST_DATA_FOLDER, TRANSFORMED_MINMAX_MIN_TEST_FILE)))
        self.assertEqual(expected_transformation[0],actual_transformation[0])

    def test_given_fitted_processor_when_config_max_nb_then_remove_words_with_more_than_max_quantile(self):
        config_file = [{'max_df': 0.9}]
        processor = SentimentPreprocessor(config_file)
        fitting_docs = load(open(join(TEST_DATA_FOLDER, FITTING_TEST_FILE)))
        processor.fit(fitting_docs)

        test_docs = load(open(join(TEST_DATA_FOLDER, MINMAX_TEST_FILE)))

        actual_transformation = processor.transform(test_docs)

        expected_transformation = load(open(join(TEST_DATA_FOLDER, TRANSFORMED_MINMAX_MAX_TEST_FILE)))
        self.assertEqual(expected_transformation[0],actual_transformation[0])

    def test_when_training_with_corpus_then_correct_word_frequency_is_set(self):
        config_file = []
        processor = SentimentPreprocessor(config_file)
        fitting_docs = load(open(join(TEST_DATA_FOLDER,FITTING_TEST_FILE)))

        processor.fit(fitting_docs)

        self.assertEqual(8, processor.word_count['hello'])
        self.assertEqual(4, processor.word_count['mother'])

        #check for stopwords....

    def test_when_config_pos_then_keep_words_with_pos_in_config(self):
        config_file = [{'pos': ['NOUN',"VERB","ADJ"]}]
        processor = SentimentPreprocessor(config_file)
        test_docs = load(open(join(TEST_DATA_FOLDER,POS_TEST_FILE)))

        actual_transformation = processor.transform(test_docs)

        expected_transformation = load(open(join(TEST_DATA_FOLDER, TRANSFORMED_POS_TEST_FILE)))
        self.assertEqual(expected_transformation[0], actual_transformation[0])
