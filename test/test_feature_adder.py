from unittest import TestCase
from json import load
from os.path import join

from feature_adder import FeatureAdder

TEST_DATA_FOLDER = "./test/data/feature"

FEATURE_ADDER_TEST_FILE = 'feature_adder_test_data.json'

UNIGRAM_TRANSFORMED_TEST_FILE = 'unigram_feature_adder_test_data.json'
BIGRAM_TRANSFORMED_TEST_FILE = 'bigram_feature_adder_test_data.json'
UNI_BIGRAM_TRANSFORMED_TEST_FILE = 'uni_bigram_feature_adder_test_data.json'

class TestFeatureAdder(TestCase):

    TARGET_1 = "target_1"
    TARGET_2 = "target_2"

    def test_when_tranform_then_create_array_of_tuple_with_feature_and_answers(self):
        feature_adder = FeatureAdder([])

        features = feature_adder.transform(["", ""],[self.TARGET_1, self.TARGET_2])

        self.assertEqual(2, len(features))
        self.assertTrue(hasattr(features,'__iter__'))
        self.assertTrue(isinstance(features[0],tuple))
        self.assertEqual(features[0][1], self.TARGET_1)
        self.assertEqual(features[1][1], self.TARGET_2)

    def test_when_config_unigram_then_unigram_are_added(self):
        feature_adder = FeatureAdder([{'unigram':True}])
        text_to_feature = load(open(join(TEST_DATA_FOLDER,FEATURE_ADDER_TEST_FILE)))

        actual_features = feature_adder.transform(text_to_feature, [""])[0][0]

        expected_feature = load(open(join(TEST_DATA_FOLDER,UNIGRAM_TRANSFORMED_TEST_FILE)))
        self.assertDictEqual(expected_feature,actual_features)


    def test_when_config_bigram_then_bigram_are_added(self):
        feature_adder = FeatureAdder([{'bigram':True}])
        text_to_feature = load(open(join(TEST_DATA_FOLDER,FEATURE_ADDER_TEST_FILE)))

        actual_features = feature_adder.transform(text_to_feature, [""])[0][0]

        expected_feature = load(open(join(TEST_DATA_FOLDER,BIGRAM_TRANSFORMED_TEST_FILE)))
        self.assertDictEqual(expected_feature,actual_features)


    def test_when_config_unigram_and_bigram_then_unigram_and_bigram_are_added(self):
        feature_adder = FeatureAdder([{'unigram':True},{'bigram':True}])
        text_to_feature = load(open(join(TEST_DATA_FOLDER,FEATURE_ADDER_TEST_FILE)))

        actual_features = feature_adder.transform(text_to_feature, [""])[0][0]

        expected_feature = load(open(join(TEST_DATA_FOLDER,UNI_BIGRAM_TRANSFORMED_TEST_FILE)))
        self.assertDictEqual(expected_feature,actual_features)
