import re

class Field:

    source = None
    country_source = None

    beggining_token = None
    extraction_token = None
    end_token = None

    def __init__(self, html_document):
        self.source = html_document

    def _get_country_sub_document(self, country):
        country_extraction_token = re.search("({0}<\\/a>)[\\s\\S]+?(_region)".format(country), self.source, re.IGNORECASE)

        if country_extraction_token:
            self.country_source = self.source[country_extraction_token.start(1):country_extraction_token.end(2)]
        else:
            raise Exception("le pays n'est pas dans la liste")

    def _extract_field_value(self):
        reg = re.compile("{0}{1}{2}".format(self.beggining_token, self.extraction_token, self.end_token))
        matching = reg.search(self.country_source,re.IGNORECASE)
        return self._generate_output_value(matching)

    def _generate_output_value(self, matching):
        return matching.group(1)

    def get_value(self, country):
        self._get_country_sub_document(country)
        return self._extract_field_value()


class Background(Field):
    #print_2028.html
    beggining_token = '<td class="category_data"  valign="top" style="padding-left:5px;">'
    extraction_token = '([^<]+)'
    end_token = "</td>"


class Geography(Field):
    #print_2113.html

    beggining_token = '<td class="category_data"  valign="top" style="padding-left:5px;">'
    extraction_token = '([^<]+)'
    end_token = "</td>"


class Economy(Field):
    #print_2116.html

    beggining_token = '<td class="category_data"  valign="top" style="padding-left:5px;">'
    extraction_token = '([^<]+)'
    end_token = "</td>"




