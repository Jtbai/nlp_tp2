from nltk.tokenize import TreebankWordTokenizer
from nltk.stem import SnowballStemmer, PorterStemmer
from nltk.corpus import stopwords
from nltk import FreqDist
from nltk import pos_tag

STEMMER_PORTER = 1
STEMMER_SNOWBALL = 2

STOPWORD_ENGLISH = 3

class SentimentPreprocessor:
    tokenizer = TreebankWordTokenizer()

    def __init__(self, config_json):
        self.config_json = config_json

    def fit(self, x):
        word_doc = []
        for sentense in x:
            for word in self.tokenizer.tokenize(sentense):
                word_doc.append(word.lower())

        self.word_count = FreqDist(word_doc)
        max_freq = max(self.word_count.values())
        self.word_percentil = {}

        for word in set(word_doc):
            self.word_percentil[word] = self.word_count.get(word, 0) / max_freq

        return self

    def transform(self, docs):
        transformed_docs = []
        for doc in docs:
            current_doc = doc
            for task_description in self.config_json:

                task = list(task_description.keys())[0]
                if task == "stemming":
                    current_doc = self._stemming(current_doc,task_description[task])
                if task == "stopword":
                    current_doc = self._stopwords(current_doc)
                if task == "pos":
                    current_doc = self._pos(current_doc,task_description[task])

            transformed_docs.append(current_doc)

        self.fit(transformed_docs)
        doc_to_output = []
        for doc in transformed_docs:
            current_doc = doc
            for task_description in self.config_json:

                if task == "min_df":
                    current_doc = self._mindf(current_doc, task_description[task])
                if task == "max_df":
                    current_doc = self._maxdf(current_doc, task_description[task])

            doc_to_output.append(current_doc)
        self.fit(doc_to_output)
        return doc_to_output

    def _stemming(self, doc, config=STEMMER_PORTER):
        if config == STEMMER_PORTER:
            stemmer = PorterStemmer()
        elif config == STEMMER_SNOWBALL:
            stemmer = SnowballStemmer('english')
        else:
            raise ValueError("Incorrect stemmer")

        return " ".join([stemmer.stem(word) for word in self.tokenizer.tokenize(doc)])

    def _mindf(self, doc, config=1):
        return " ".join([word for word in self.tokenizer.tokenize(doc) if self.word_count[word] > config])

    def _maxdf(self, doc, config=1):
        return " ".join([word for word in self.tokenizer.tokenize(doc) if self.word_percentil.get(word, 0) <= config])

    def _stopwords(self, doc, config=STOPWORD_ENGLISH):
        sw = None
        if config == STOPWORD_ENGLISH:
            sw = stopwords.words('english')

        return " ".join([word for word in self.tokenizer.tokenize(doc) if word not in sw])

    def _pos(self, doc, config=["NOUN", "ADJ", "ADV", "VERB"]):
        posed_doc = pos_tag(self.tokenizer.tokenize(doc),'universal')
        return " ".join([word[0] for word in posed_doc if word[1] in config])

