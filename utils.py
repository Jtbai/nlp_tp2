from os.path import join
from os import walk
from fields import *
import re


def get_file_data_path(root, prefix='', suffix=''):
    for _, _, file_list in walk(root):
        for docs in file_list:
            if docs[:len(prefix)] == prefix and docs[len(suffix)-1:]:
                yield docs

def generate_file_field_mapping(path, file_list, output_file):
    field_name_template = ':: ((\w+[\s,-]*)+\w*)'

    with open(output_file, 'w') as output:
        for field_document in file_list:
            with open(join(path, field_document)) as f:
                html_document = f.read()

            field = re.search(field_name_template, html_document).group(1)
            output.write("{0}; {1}\n".format(field_document, field))

def convert_selection_string_to_field_file(selection):

    selection_to_field = {
        "BACKGROUND": "print_2028.html",
        "GEOGRAPHY": "print_2113.html",
        "ECONOMY": "print_2116.html",
    }

    if selection in selection_to_field.keys():
        return selection_to_field[selection]
    else:
        raise Exception("Champs non disponible a la recherche: {0}".format(selection))

def convert_selection_string_field_selector(selection):

    selection_to_field = {
        "BACKGROUND": Background,
        "GEOGRAPHY": Geography,
        "ECONOMY": Economy,
    }

    if selection in selection_to_field.keys():
        return selection_to_field[selection]
    else:
        raise Exception("Champs non disponible a la recherche: {0}".format(selection))

def get_full_file_text(path, file_name):
    with open(join(path, file_name),'r') as f:
        return f.read()
