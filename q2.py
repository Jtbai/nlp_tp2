from sklearn.feature_extraction.text import CountVectorizer,TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score, precision_recall_fscore_support
vectorizer = CountVectorizer()
transformer = TfidfTransformer()
classifier = MultinomialNB()

from sklearn.pipeline import Pipeline
pipeline = Pipeline([('vectotorizer', vectorizer),('classifier', classifier)])


data = open('./data/q2/questions.txt')
x = []
y = []
for line in data:
    split_line = line.split(" ")
    y.append(split_line[0])
    x.append(" ".join(split_line[1:]).strip())

from sklearn.model_selection import train_test_split

accuracies = []
recall_precision = []

for i in range(10):
    x_train, x_test, y_train,  y_test = train_test_split(x,y,random_state=12*i,test_size=20)

    pipeline.fit(x_train,y_train)
    results = pipeline.predict(x_test)

    current_accuracy = accuracy_score(y_test, results)
    current_recall_precision = precision_recall_fscore_support(y_test, results)


    print(current_accuracy)
    metrics = [sum(x * current_recall_precision[-1])/sum(current_recall_precision[-1]) for x in current_recall_precision[:-1] ]

    accuracies.append(current_accuracy)
    recall_precision.append(metrics)

    print(metrics)
    print(confusion_matrix(y_test,results))

    for index, corresponding_pair in enumerate(zip(y_test,results)):
        truth, predicted = corresponding_pair
        if truth != predicted:
            print("{} instead of {} for : {}".format(predicted,truth,x_test[index]) )

print("Global accuracy: {}".format(sum(accuracies)/len(accuracies)))
print("Global Recall: {}".format(sum([x[0] for x in recall_precision])/len(accuracies)))
print("Global Precision: {}".format(sum([x[1] for x in recall_precision])/len(accuracies)))
print("Global F1: {}".format(sum([x[2] for x in recall_precision])/len(accuracies)))
