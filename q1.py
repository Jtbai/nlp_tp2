from os.path import join
from utils import convert_selection_string_to_field_file, get_full_file_text, convert_selection_string_field_selector
from elasticsearch import Elasticsearch

def obtain_world_factbook_documents():
    HTML_DOCS_LOCATION = './data/factbook/fields'
    COUNTRY_LIST_FILE = './data/q1/country_list.txt'

    fields_to_index = ["BACKGROUND","GEOGRAPHY","ECONOMY"]

    doc_to_index = {}
    for field_name in fields_to_index:
        field_file = convert_selection_string_to_field_file(field_name)
        html_doc = get_full_file_text(HTML_DOCS_LOCATION, field_file)
        field = convert_selection_string_field_selector(field_name)(html_doc)
        with open(COUNTRY_LIST_FILE) as country_list:
            for country in country_list:
                current_doc = doc_to_index.get(country.strip(),{'name':country.strip()})
                try:
                    current_doc[field_name] = field.get_value(country.strip()).strip()
                    doc_to_index[country.strip()] = current_doc
                except Exception as e:
                    pass
                    # print("{}: {} ({})".format(field_name,country.strip(),e.args))
    return doc_to_index

def index_documnt(id, the_document):
    es = Elasticsearch()
    return es.index(index="factbook", doc_type='structured_text', id=id, body=the_document)

def index_all_documents(documents):
    for id, doc in enumerate(documents.values()):
        index_documnt(id,doc)

def get_queries(file_stream):
    queries = {}
    for line in file_stream:
        splited_line = line.split("\t")
        queries[splited_line[0]] = " ".join(splited_line[1:]).strip()

    return queries

def get_jugements(file_stream):
    jugements = {}
    for line in file_stream:
        splited_line = line.split(" ")
        jugment_list = jugements.get(splited_line[0], [])
        jugment_list.append(" ".join(splited_line[1:]).strip())
        jugements[splited_line[0]] = jugment_list
    return jugements

def search_request(query):
    es = Elasticsearch()
    res = es.search(index="factbook", body={ "size" : 1000,
                                          "query": {
                                              "multi_match": {
                                                  "query": query,
                                                  "fields": ["BACKGROUND","GEOGRAPHY", "ECONOMY"]
                                              }
                                          }
                                        })
    return [x['_source']['name'] for x in res['hits']['hits']]

def calculate_mean_average_precision(truth, found):
    truth_ranks = []
    for current_truth in truth:
        if current_truth in found:
            truth_index_in_found = found.index(current_truth)
            truth_rank_in_found = truth_index_in_found+1
            truth_ranks.append(truth_rank_in_found )

        truth_ranks.sort()
    average_precision = 0
    for index, truth_rank in enumerate(truth_ranks):
        rank = index+1
        average_precision += rank/truth_rank

    denominator = len(truth)
    return average_precision/ denominator


REQUETE_FILE = 'liste_requetes.txt'
JUGEMENTS_FILE = 'jugements.txt'
DATA_PATH = join('data','q1')

queries = get_queries(open(join(DATA_PATH,REQUETE_FILE)))
jugements = get_jugements(open(join(DATA_PATH,JUGEMENTS_FILE)))

accuracy_sum = []
for id, query in queries.items():
    found = search_request(query)
    truth = jugements[id]

    print("Nb found: {}/{} (out of {})".format(sum([1 for x in truth if x in found]),len(truth),len(found)))
    current_accuracy = round(calculate_mean_average_precision(truth,found)*100,0)
    accuracy_sum.append(current_accuracy)
    print("{} {} : {}%".format(id, query,current_accuracy))

print("{}".format(sum(accuracy_sum)/len(accuracy_sum)))

# print("Got %d Hits:" % res['hits']['total'])
# for hit in res['hits']['hits']:
#     print("{} {}: {}".format(hit['_id'],hit["_source"]['name'],hit['_score']))

