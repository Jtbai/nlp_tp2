from nltk.tokenize import TreebankWordTokenizer

N_GRAM_BINARY = 1
N_GRAM_COUNT = 2

class FeatureAdder:

    tokenizer = TreebankWordTokenizer()

    def __init__(self, config):
        self.config = config

    def transform(self, x, y):
        transformed_docs = []
        for doc, target in zip(x,y):

            features = {}
            for task_description in self.config:
                task = list(task_description.keys())[0]
                if task == "unigram":
                    for current_feature, weight in self._unigram(doc).items():
                        features[current_feature] = features.get(current_feature, 0) + weight
                if task == "bigram":
                    for current_feature, weight in self._bigram(doc).items():
                        features[current_feature] = features.get(current_feature, 0) + weight

            current_doc = (features, target)
            transformed_docs.append(current_doc)

        return transformed_docs

    def _unigram(self,x,config=N_GRAM_BINARY):
        features = {}
        for word in self.tokenizer.tokenize(x):
            feature_name = "contains({})".format(word)

            if config == N_GRAM_BINARY:
                actual_weight = 0
            elif config == N_GRAM_COUNT:
                actual_weight = features.get(word,0)
            else:
                raise ValueError("unigram count patern not recognized")

            features[feature_name] = actual_weight + 1

        return features


    def _bigram(self,x,config=N_GRAM_BINARY):
        features = {}
        padded_tokened_x = self.__add_padding(self.tokenizer.tokenize(x),1)
        old_word = None
        for word in padded_tokened_x :
            if old_word:
                feature_name = "contains({} {})".format(old_word, word)

                if config == N_GRAM_BINARY:
                    actual_weight = 0
                elif config == N_GRAM_COUNT:
                    actual_weight = features.get(word,0)
                else:
                    raise ValueError("unigram count patern not recognized")

                features[feature_name] = actual_weight + 1
            old_word = word
        return features

    def __add_padding(self, tokened_x, count):
        start_token = "<start>"
        end_token = "<end>"
        output_tokens = tokened_x
        for index in range(count):
            output_tokens.insert(0,start_token)
            output_tokens.append(end_token)

        return output_tokens